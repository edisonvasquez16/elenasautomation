package co.com.elenas.steps;

import co.com.elenas.factories.OptionFactory;
import co.com.elenas.navigation.OpenBrowser;
import co.com.elenas.questions.NumberObjects;
import co.com.elenas.questions.TextsFounds;
import co.com.elenas.sessionvariables.ValueOptions;
import co.com.elenas.tasks.VoteBy;
import co.com.elenas.tasks.VoteByAll;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.elenas.ui.ResultElenasPage.LBL_RESULTS;
import static co.com.elenas.ui.ResultElenasPage.LNK_VOTE_AGAIN;
import static co.com.elenas.ui.TestElenasPage.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.*;
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the;
import static org.hamcrest.Matchers.*;

public class TestElenasSteps {

    @Dado("^que el usuario (.*) ya ha realizado el acceso hasta la aplicación$")
    public void accessToAplication(String name) {
        theActorCalled(name).wasAbleTo(OpenBrowser.inTestPage());
    }

    @Cuando("^el usuario vota por la opción (.*)$")
    public void voteByOption(String option) {
        theActorInTheSpotlight()
                .wasAbleTo(
                        VoteBy.option(
                                OptionFactory.create(option)));
    }

    @Cuando("^el no selecciona ninguna opción de las presentes$")
    public void notSelectChoice() {
        theActorInTheSpotlight()
                .wasAbleTo(
                        Click.on(BTN_VOTE));
    }

    @Cuando("^el selecciona y vota por todas las opciones$")
    public void voteByAllOption() {
        theActorInTheSpotlight()
                .wasAbleTo(
                        VoteByAll.options());
    }

    @Entonces("^el debería poder utilizar el formulario para votar$")
    public void viewOptionsToVote() {
        theActorInTheSpotlight()
                .should(
                        seeThat(TextsFounds.in(RADIOS_ALL_OPTIONS),
                                hasItems("Not much", "The sky", "La la la"))
                );
    }

    @Entonces("^el puede votar correctamente$")
    public void viewVotingResults() {
        theActorInTheSpotlight()
                .should(
                        seeThat(NumberObjects.foundWith(LBL_RESULTS), greaterThanOrEqualTo(3)),
                        seeThat(the(LNK_VOTE_AGAIN), isPresent())
                );
    }

    @Entonces("^el puede votar correctamente por todas las opciones$")
    public void viewVotingAllResults() {
        theActorInTheSpotlight()
                .should(
                        seeThat(NumberObjects.foundWith(LBL_RESULTS), greaterThanOrEqualTo(3)),
                        seeThat(TextsFounds.in(LBL_RESULTS), not(theActorInTheSpotlight().recall(ValueOptions.NotMuch.toString()).toString())),
                        seeThat(TextsFounds.in(LBL_RESULTS), not(theActorInTheSpotlight().recall(ValueOptions.TheSky.toString()).toString())),
                        seeThat(TextsFounds.in(LBL_RESULTS), not(theActorInTheSpotlight().recall(ValueOptions.Lalala.toString()).toString())),
                        seeThat(the(LNK_VOTE_AGAIN), isPresent())
                );
    }

    @Entonces("^el no debería poder continuar con el flujo hasta seleccionar una opción$")
    public void notViewVotingResults() {
        theActorInTheSpotlight()
                .should(
                        seeThat(the(LBL_SELECT_CHOICE), isVisible()),
                        seeThat(the(LNK_VOTE_AGAIN), isNotPresent())
                );
    }
}
