#Creado por Edison Vasquez Burbano
#language: es

Característica: Flujo de prueba Elenas
  Como un usuario del sistema
  Quiero usar el cuestionario
  Para votar acerca

  Antecedentes: Acceso hasta la aplicación
    Dado que el usuario Test ya ha realizado el acceso hasta la aplicación

  @ValidateFields @smoke
  Escenario: Validar opciones para votar
    Entonces el debería poder utilizar el formulario para votar

  @OptionsVote @smoke
  Esquema del escenario: Votar por las opciones disponibles
    Cuando el usuario vota por la opción <opción>
    Entonces el puede votar correctamente

    Ejemplos:
      | opción   |
      | Not much |
      | The sky  |
      | La la la |

  @NotSelectOption @smoke
  Escenario: Votar sin seleccionar ninguna opción
    Cuando el no selecciona ninguna opción de las presentes
    Entonces el no debería poder continuar con el flujo hasta seleccionar una opción

  @VoteForAllOptions @smoke
  Escenario: Votar por las tres opciones
    Cuando el selecciona y vota por todas las opciones
    Entonces el puede votar correctamente por todas las opciones
