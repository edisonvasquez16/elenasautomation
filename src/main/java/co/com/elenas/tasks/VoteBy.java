package co.com.elenas.tasks;

import co.com.elenas.models.OptionModel;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.elenas.ui.TestElenasPage.BTN_VOTE;
import static co.com.elenas.ui.TestElenasPage.RADIO_OPTION;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class VoteBy implements Task {

    private final OptionModel option;

    public VoteBy(OptionModel option) {
        this.option = option;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(RADIO_OPTION.of(option.getName())),
                Click.on(BTN_VOTE)
        );
    }

    public static VoteBy option(OptionModel option) {
        return instrumented(VoteBy.class, option);
    }

}
