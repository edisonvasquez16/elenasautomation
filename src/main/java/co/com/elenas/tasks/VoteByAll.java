package co.com.elenas.tasks;

import co.com.elenas.questions.TextFound;
import co.com.elenas.sessionvariables.ValueOptions;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.elenas.ui.ResultElenasPage.LBLS_RESULTS;
import static co.com.elenas.ui.ResultElenasPage.LNK_VOTE_AGAIN;
import static co.com.elenas.ui.TestElenasPage.BTN_VOTE;
import static co.com.elenas.ui.TestElenasPage.RADIOS_OPTION;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class VoteByAll implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(RADIOS_OPTION.of(String.valueOf(1))),
                Click.on(BTN_VOTE)
        );
        actor.remember(ValueOptions.NotMuch.toString(), TextFound.in(LBLS_RESULTS.of("1")));
        actor.remember(ValueOptions.TheSky.toString(), TextFound.in(LBLS_RESULTS.of("2")));
        actor.remember(ValueOptions.Lalala.toString(), TextFound.in(LBLS_RESULTS.of("3")));
        actor.attemptsTo(Click.on(LNK_VOTE_AGAIN));
        for (int x = 1; x <= 3; x++) {
            actor.attemptsTo(
                    Click.on(RADIOS_OPTION.of(String.valueOf(x))),
                    Click.on(BTN_VOTE)
            );
            if (x < 3)
                actor.attemptsTo(Click.on(LNK_VOTE_AGAIN));
        }
    }

    public static VoteByAll options() {
        return instrumented(VoteByAll.class);
    }
}
