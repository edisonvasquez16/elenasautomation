package co.com.elenas.factories;

import co.com.elenas.models.OptionModel;

public class OptionFactory {

    public static OptionModel create(String name) {
        OptionModel option = new OptionModel();
        option.setName(name);
        return option;
    }

}
