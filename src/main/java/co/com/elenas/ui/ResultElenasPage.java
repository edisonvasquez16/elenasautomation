package co.com.elenas.ui;

import net.serenitybdd.screenplay.targets.Target;

public class ResultElenasPage {

    public static final Target LBLS_RESULTS = Target.the("Results").locatedBy("//li[{0}]");
    public static final Target LBL_RESULTS = Target.the("Results").locatedBy("//li");
    public static final Target LNK_VOTE_AGAIN = Target.the("Link vote agaín").locatedBy("//a[@href]");

}
