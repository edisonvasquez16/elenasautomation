package co.com.elenas.ui;

import net.serenitybdd.screenplay.targets.Target;

public class TestElenasPage {

    public static final Target RADIOS_OPTION = Target.the("Option for vote").locatedBy("//input[@type='radio'][{0}]");
    public static final Target RADIO_OPTION = Target.the("Option for vote").locatedBy("//label[@for and contains(text(),'{0}')]");
    public static final Target RADIOS_ALL_OPTIONS = Target.the("Option for vote").locatedBy("//label[@for]");
    public static final Target BTN_VOTE = Target.the("Vote button").locatedBy("//input[@value='Vote']");
    public static final Target LBL_SELECT_CHOICE = Target.the("Message validation select choice").locatedBy("//strong[contains(text(),'select a choice.')]");

}
