package co.com.elenas.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Visibility;
import net.serenitybdd.screenplay.targets.Target;

public class NumberObjects implements Question<Integer> {

    private final Target target;

    public NumberObjects(Target target) {
        this.target = target;
    }

    public static Question<Integer> foundWith(Target target) {
        return new NumberObjects(target);
    }

    @Override
    public Integer answeredBy(Actor actor) {
        return Visibility.of(target).viewedBy(actor).asList().size();
    }
}
