package co.com.elenas.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.targets.Target;

import java.util.List;

public class TextsFounds implements Question<List<String>> {

    private final Target target;

    public TextsFounds(Target target) {
        this.target = target;
    }

    public static Question<List<String>> in(Target target) {
        return new TextsFounds(target);
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(target).viewedBy(actor).asList();
    }
}
